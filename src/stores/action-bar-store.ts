import { defineStore, storeToRefs } from 'pinia';

export interface ActionBar {
  icon: string;
  label: string;
  click: () => void;
}

export const useActionBarStore = defineStore('actionBar', {
  state: () => ({ actionBars: [] as ActionBar[] }),

  getters: {
    actions(state) {
      return state.actionBars;
    }
  },

  actions: {
    setActions(actionBars: ActionBar[]) {
      this.actionBars = actionBars;
    }
  }
});

export const actionBarRef = () => storeToRefs(useActionBarStore());
