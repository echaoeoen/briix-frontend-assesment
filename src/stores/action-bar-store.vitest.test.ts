import { setActivePinia, createPinia } from 'pinia';
import { useActionBarStore } from './action-bar-store';
import { describe, expect, it, beforeEach } from 'vitest';

describe('ActionBar Store', () => {
  beforeEach(() => {
    // creates a fresh pinia and makes it active
    // so it's automatically picked up by any useStore() call
    // without having to pass it to it: `useStore(pinia)`
    setActivePinia(createPinia());
  });

  it('setActions', () => {
    const store = useActionBarStore();
    expect(store.actionBars).toEqual([]);
    store.setActions([
      {
        click: () => {
          // do something
        },
        icon: 'add',
        label: 'some label'
      }
    ]);
    expect(store.actionBars).toHaveLength(1);
    expect(store.actionBars[0].icon).toBe('add');
  });
});
