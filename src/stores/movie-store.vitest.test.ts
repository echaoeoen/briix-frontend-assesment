import { setActivePinia, createPinia } from 'pinia';
import { describe, expect, it, beforeEach } from 'vitest';
import { useMovieStore } from './movie-store';
import { moviesData } from 'src/schemas/movies-seed';

describe('Movie Store', () => {
  beforeEach(() => {
    // creates a fresh pinia and makes it active
    // so it's automatically picked up by any useStore() call
    // without having to pass it to it: `useStore(pinia)`
    setActivePinia(createPinia());
  });

  it('should initiated first state', () => {
    const store = useMovieStore();
    expect(store.movies).toEqual({
      '0': {
        id: 0,
        title: 'Beetlejuice',
        genres: ['Sci-Fi'],
        director: 'Tim Burton',
        summary:
          'A couple of recently deceased ghosts contract the services of a "bio-exorcist" in order to remove the obnoxious new owners of their house.',
        posterUrl:
          'https://posters.movieposterdb.com/23_02/1991/6692194/l_beetlejuice-movie-poster_a3b62eee.jpg'
      },
      '1': {
        id: 1,
        title: 'The Shawshank Redemption',
        genres: ['Drama', 'Action'],
        director: 'Frank Darabont',
        summary:
          'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
        posterUrl:
          'https://posters.movieposterdb.com/05_03/1994/0111161/l_8494_0111161_3bb8e662.jpg'
      },
      '2': {
        id: 2,
        title: 'Valkyrie',
        genres: ['Drama'],
        director: 'Bryan Singer',
        summary:
          'A dramatization of the 20 July assassination and political coup plot by desperate renegade German Army officers against Hitler during World War II.',
        posterUrl:
          'https://posters.movieposterdb.com/09_01/2008/985699/l_985699_d61d0101.jpg'
      },
      '3': {
        id: 3,
        title: 'Ratatouille',
        genres: ['Animation'],
        director: 'Brad Bird, Jan Pinkava',
        summary:
          'A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.',
        posterUrl:
          'https://posters.movieposterdb.com/07_11/2007/382932/l_382932_bcae738b.jpg'
      }
    });
  });
  describe('add', () => {
    it('should add a movie', () => {
      const store = useMovieStore();
      store.add({
        director: 'test',
        genres: ['test'],
        posterUrl: 'test',
        summary: 'test',
        title: 'test'
      });
      expect(store.movies[4]).toEqual({
        director: 'test',
        genres: ['test'],
        posterUrl: 'test',
        summary: 'test',
        title: 'test',
        id: 4
      });
    });
  });
  describe('update', () => {
    it('should should update a movie', () => {
      const store = useMovieStore();
      store.update(4, {
        director: 'updated',
        genres: ['updated'],
        posterUrl: 'updated',
        summary: 'updated',
        title: 'updated'
      });
      expect(store.movies[4]).toEqual({
        director: 'updated',
        genres: ['updated'],
        posterUrl: 'updated',
        summary: 'updated',
        title: 'updated',
        id: 4
      });
    });
  });
  describe('delete', () => {
    it('should should delete a movie', () => {
      const store = useMovieStore();
      store.delete(4);
      expect(store.movies[4]).toBeUndefined();
    });
  });
});
