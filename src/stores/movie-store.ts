import { defineStore, storeToRefs } from 'pinia';
import { Movie } from 'src/schemas/movie';
import { useStorage } from 'src/services/local-storage';
const db = useStorage<Movie>('movies');

export const useMovieStore = defineStore('movies', {
  state: () => ({ movies: db.getAll() }),

  getters: {
    actions(state) {
      return state.movies;
    }
  },

  actions: {
    add(movie: Movie) {
      db.append(movie);
      this.movies = db.getAll();
    },
    refresh() {
      this.movies = db.getAll();
    },
    delete(id: number) {
      db.delete(id);
      this.movies = db.getAll();
    },
    update(id: number, movie: Movie) {
      db.update(id, movie);
      this.movies = db.getAll();
    }
  }
});

export const movieRef = () => storeToRefs(useMovieStore());
