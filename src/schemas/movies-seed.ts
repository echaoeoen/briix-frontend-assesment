import { Movie } from './movie';

export const moviesData: Movie[] = [
  {
    title: 'Beetlejuice',
    genres: ['Sci-Fi'],
    director: 'Tim Burton',
    summary:
      'A couple of recently deceased ghosts contract the services of a "bio-exorcist" in order to remove the obnoxious new owners of their house.',
    posterUrl:
      'https://posters.movieposterdb.com/23_02/1991/6692194/l_beetlejuice-movie-poster_a3b62eee.jpg'
  },
  {
    title: 'The Shawshank Redemption',
    genres: ['Drama', 'Action'],
    director: 'Frank Darabont',
    summary:
      'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
    posterUrl:
      'https://posters.movieposterdb.com/05_03/1994/0111161/l_8494_0111161_3bb8e662.jpg'
  },

  {
    title: 'Valkyrie',
    genres: ['Drama'],
    director: 'Bryan Singer',
    summary:
      'A dramatization of the 20 July assassination and political coup plot by desperate renegade German Army officers against Hitler during World War II.',
    posterUrl:
      'https://posters.movieposterdb.com/09_01/2008/985699/l_985699_d61d0101.jpg'
  },
  {
    title: 'Ratatouille',
    genres: ['Animation'],
    director: 'Brad Bird, Jan Pinkava',
    summary:
      'A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.',
    posterUrl:
      'https://posters.movieposterdb.com/07_11/2007/382932/l_382932_bcae738b.jpg'
  }
];
