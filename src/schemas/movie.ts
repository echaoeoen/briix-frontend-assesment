export interface Movie {
  title: string;
  summary: string;
  director: string;
  genres: string[];
  posterUrl?: string;
}
