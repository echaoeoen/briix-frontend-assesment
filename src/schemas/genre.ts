export const genres = [
  'Action',
  'Animation',
  'Drama',
  'Sci-Fi',
  'Comedy',
  'Horror',
  'Romance',
  'Adventure',
  'Fantasy',
  'Thriller'
];
