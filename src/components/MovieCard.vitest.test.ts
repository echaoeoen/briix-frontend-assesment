import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';
import MovieCard from './MovieCard.vue';
import { moviesData } from 'src/schemas/movies-seed';

installQuasarPlugin();

describe('MovieCard Component', () => {
  it('should mount component with props', () => {
    const wrapper = mount(MovieCard, {
      props: { movie: { id: 1, ...moviesData[0] } }
    });

    expect(wrapper.html()).contain(moviesData[0].title);
    expect(wrapper.html()).contain(moviesData[0].summary);
    expect(wrapper.html()).contain(moviesData[0].director);
    expect(wrapper.html()).contain(moviesData[0].genres[0]);
  });
});
