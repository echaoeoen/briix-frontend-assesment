import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { shallowMount } from '@vue/test-utils';
import { describe, it, beforeAll, vi, expect } from 'vitest';
import IndexPage from './IndexPage.vue';
import { TestingPinia, createTestingPinia } from '@pinia/testing';
import { fn } from '@vitest/spy';

installQuasarPlugin();
import { useActionBarStore } from '../stores/action-bar-store';

describe('MovieCard Component', () => {
  let pinia: TestingPinia;
  let actionBarStore;
  const setActionMocked = fn();
  beforeAll(() => {
    pinia = createTestingPinia({
      createSpy: vi.fn,
      initialState: {
        main: {
          count: 0
        }
      }
    });
    actionBarStore = useActionBarStore();
    setActionMocked.mockReturnValue(undefined);
    actionBarStore.setActions = setActionMocked();
    actionBarStore.actionBars = [];
  });
  it('should mount component with props', () => {
    const wrapper = shallowMount(IndexPage, {
      global: {
        plugins: [pinia]
      }
    });
    expect(wrapper.html()).toContain('movie-card');
    const movieCard = wrapper.findAllComponents({ name: 'MovieCard' });
    expect(movieCard).toHaveLength(4);
  });
  it('should mount component and filter movie cards when search is filled', async () => {
    const wrapper = shallowMount(IndexPage, {
      global: {
        plugins: [pinia]
      }
    });

    const searchInput = wrapper.findComponent({ name: 'QInput' });
    searchInput.setValue('rat');
    await searchInput.trigger('input');
    expect(wrapper.html()).toContain('movie-card');
    const movieCard = wrapper.findAllComponents({ name: 'MovieCard' });
    expect(movieCard).toHaveLength(1);
  });
});
