import { init } from './init-movies';

export type StorageSchema<T> = T & { id: number };
init();
export const useStorage = <T>(db: string) => {
  const getAll = () => {
    const dataStr = localStorage.getItem(db);
    const data = JSON.parse(dataStr ?? '{}') as Record<
      number,
      StorageSchema<T>
    >;
    return data;
  };
  return {
    getAll,
    append: (item: T) => {
      const data = getAll();
      const newId = Object.keys(data).length;
      data[newId] = { id: newId, ...item };
      localStorage.setItem(db, JSON.stringify(data));
      return newId;
    },
    delete: (id: number) => {
      const data = getAll();
      delete data[id];
      localStorage.setItem(db, JSON.stringify(data));
    },
    update: (id: number, item: T) => {
      const data = getAll();
      delete data[id];
      data[id] = { id, ...item };
      localStorage.setItem(db, JSON.stringify(data));
    }
  };
};
