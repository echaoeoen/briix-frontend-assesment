import { moviesData } from 'src/schemas/movies-seed';
import { StorageSchema } from './local-storage';
import { Movie } from 'src/schemas/movie';

export const init = () => {
  const isInit = localStorage.getItem('isInit');
  if (!isInit) {
    localStorage.setItem(
      'movies',
      JSON.stringify(
        moviesData.reduce(
          (acc, movie, index) => ({
            ...acc,
            [index]: {
              id: index,
              ...movie
            }
          }),
          {} as Record<string, StorageSchema<Movie>>
        )
      )
    );
    localStorage.setItem('isInit', 'true');
  }
};
