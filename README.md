# briix-assesment (briix-assesment)

A Quasar Project

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```
you will see this text on the console
```bash script
 App • Opening default browser at http://localhost:9000/
```
then open the url `http://localhost:port`

### Lint the files
```bash
yarn lint
# or
npm run lint
```

### Test
#### Live test
to run live test on watch
```bash
yarn test
```
![plot](./readme-image/test.png)

### Format the files
```bash
yarn format
# or
npm run format
```


### Build the app for production
```bash
quasar build
```

## App Screenshoot
![plot](./readme-image/show.png)
![plot](./readme-image/create.png)
![plot](./readme-image/created-movie.png)
![plot](./readme-image/search.png)
![plot](./readme-image/update-form.png)
![plot](./readme-image/delete-confirmation.png)
